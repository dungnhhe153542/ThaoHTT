import React from "react";
import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
import TextTruncate from "react-text-truncate";
export const NavbarCmp = ({
  nameTitle,
  roleName,
  settingGroup,
  displayOrder,
  description,
  status,
  layoutCard,
}) => {
  const className = ` card custom-card task-${nameTitle}-card`;
  const taskCard = `${layoutCard} task-card`;
  return (
    <>
      <div className={taskCard}>
        <div className={className}>
          <div className="card-body">
            <div className="justify-content-between flex-wrap gap-2">
              <div>
                <p className="fw-semibold mb-3 d-flex align-items-center">
                  <a href="javascript:void(0);">
                    <i className="ri-star-s-fill fs-16 op-5 me-1 text-muted"></i>
                  </a>
                  {roleName}
                </p>
                <p className="mb-3">
                  Setting Group :{" "}
                  <span className="fs-12 mb-1 text-muted">{settingGroup}</span>
                </p>
                <p className="mb-3">
                  Display Order :{" "}
                  <span className="fs-12 mb-1 text-muted">{displayOrder}</span>
                </p>
                <p className="mb-0">
                  <span>Description: </span>
                  <span className="fs-12 mb-1 text-muted">
                    <TextTruncate
                      line={1}
                      element="span"
                      truncateText="…"
                      text={description}
                      // textTruncateChild={<a href="#">Read on</a>}
                    />
                  </span>
                </p>
              </div>
              <div>
                <div className="btn-list">
                  <button className="btn btn-sm btn-icon btn-wave btn-primary-light">
                    <EditOutlined />
                  </button>
                  <button className="btn btn-sm btn-icon btn-wave btn-danger-light me-0">
                    <DeleteOutlined />
                  </button>
                </div>
                {status === true ? (
                  <span className="badge bg-success-transparent d-block">
                    active
                  </span>
                ) : (
                  <span className="badge bg-danger-transparent d-block">
                    inactive
                  </span>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
