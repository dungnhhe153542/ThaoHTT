import React, { useState } from "react";
import "./assets/bootstrap.min.css";
import "./assets/style.min.css";
import SettingPage from "./SettingPage";
import Pending from "./Pending";
import './index.css'
const NavbarSetting = () => {
  const [tab, setTab] = useState("all");
  return (
    <>
      <div className="col-xl-9">
        <div className="row">
          <div className="col-xl-12">
            <div className="card custom-card">
              <div className="card-body p-0">
                <div className="d-flex p-3 align-items-center justify-content-between">
                  <div>
                    <h6 className="fw-semibold mb-0">Tasks</h6>
                  </div>
                  <div>
                    <ul
                      className="nav nav-tabs nav-tabs-header mb-0 d-sm-flex d-block"
                      role="tablist"
                    >
                      <li className="nav-item m-1">
                        <a
                          className="nav-link pointer active"
                          data-bs-toggle="tab"
                          onClick={()=> {
                            setTab('all')
                          }}
                        >
                          All Tasks
                        </a>
                      </li>
                      <li className="nav-item pointer m-1">
                        <a
                          className="nav-link"
                          data-bs-toggle="tab"
                          onClick={()=> {
                            setTab('pending')
                          }}
                        >
                          Pending
                        </a>
                      </li>
                      <li className="nav-item pointer m-1">
                        <a
                          className="nav-link"
                          data-bs-toggle="tab"
                          onClick={()=> {
                            setTab('inprogress')
                          }}
                        >
                          In Progress
                        </a>
                      </li>
                      <li className="nav-item pointer m-1">
                        <a
                          className="nav-link"
                          data-bs-toggle="tab"
                          onClick={()=> {
                            setTab('completed')
                          }}
                        >
                          Completed
                        </a>
                      </li>
                    </ul>
                  </div>
                  <div>
                    <div className="dropdown">
                      <button
                        className="btn btn-icon btn-sm btn-light btn-wave waves-light waves-effect"
                        type="button"
                        data-bs-toggle="dropdown"
                        aria-expanded="false"
                      >
                        <i className="ti ti-dots-vertical"></i>
                      </button>
                      <ul className="dropdown-menu">
                        <li>
                          <a
                            className="dropdown-item"
                            href="javascript:void(0);"
                          >
                            Select All
                          </a>
                        </li>
                        <li>
                          <a
                            className="dropdown-item"
                            href="javascript:void(0);"
                          >
                            Share All
                          </a>
                        </li>
                        <li>
                          <a
                            className="dropdown-item"
                            href="javascript:void(0);"
                          >
                            Delete All
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="tab-content task-tabs-container">
            <div
              className="tab-pane show active p-0"
              id="all-tasks"
              role="tabpanel"
            >
              <div className={tab === 'all' ? 'row' : 'row d-none'} id="tasks-container">
                <SettingPage />
              </div>
              <div className={tab === 'pending' ? 'row' : 'row d-none'} id="tasks-container">
                <Pending />
              </div>
              <div className={tab === 'inprogress' ? 'row' : 'row d-none'} id="tasks-container">
                <SettingPage />
              </div>
              <div className={tab === 'completed' ? 'row' : 'row d-none'} id="tasks-container">
                <Pending />
              </div>
            </div>
          </div>
        </div>
        <ul className="pagination justify-content-end">
          <li className="page-item disabled">
            <a className="page-link">Previous</a>
          </li>
          <li className="page-item">
            <a className="page-link" href="javascript:void(0);">
              1
            </a>
          </li>
          <li className="page-item">
            <a className="page-link" href="javascript:void(0);">
              2
            </a>
          </li>
          <li className="page-item">
            <a className="page-link" href="javascript:void(0);">
              3
            </a>
          </li>
          <li className="page-item">
            <a className="page-link" href="javascript:void(0);">
              Next
            </a>
          </li>
        </ul>
      </div>
    </>
  );
};

export default NavbarSetting;
